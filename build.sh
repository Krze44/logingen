#!/bin/bash

rootDirectory=$(realpath "$(dirname "$0")/../../cpp/LoginGenerator")
outputFile=$(realpath "$rootDirectory/LoginGenerator.exe")
sourceFiles=$(find "$rootDirectory/src" -name "*.cpp" -type f)
sourceFilesPaths=$(echo "$sourceFiles" | tr '\n' ' ')
nlohmannDirectory="$rootDirectory/nlohmann"

includeFlags="-I\"$nlohmannDirectory\""
includeFlags+=$(find "$nlohmannDirectory" -type d -exec bash -c 'shopt -s nullglob; shopt -s dotglob; files=("$1"/*.hpp); (( ${#files[@]} )) && echo "-I\"$1\""' bash {} \;)

compileCommand="g++ -std=c++17 -o \"$outputFile\" -I\"$rootDirectory\" $includeFlags $sourceFilesPaths"

echo "Building LoginGenerator..."

eval $compileCommand

# Create Logins.json file if it doesn't exist
jsonFilePath="$rootDirectory/data/Logins.json"
if [ ! -f "$jsonFilePath" ]; then
  defaultJsonContent='{
    "Current_ID": 0,
    "Accounts": {}
}'
  echo "$defaultJsonContent" > "$jsonFilePath"
fi

echo "Build complete."
