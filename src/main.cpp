#include "userinput/userinput.h"

int main(int argc, char *argv[])
{
  LoginGenerator loginGenerator;
  loginGenerator.run(argc, argv);

  return 0;
}