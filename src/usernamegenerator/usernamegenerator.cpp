#include "../numbergenerator/numbergenerator.h"
#include <string>
#include <cmath>
#include <iostream>

using std::ceil;
using std::cout;
using std::endl;
using std::floor;
using std::string;



string handleCaps(int length)
{
  string res;
  const string capitals = "ABCEDFGHIJKLMNOPQRTSTUVWXYZ";
  int len = capitals.size();
  while (res.size() < length)
  {
    int index = getRandomNumber(0, len - 1);
    res += capitals[index];
  }
  return res;
}

string handleNums(int length)
{
  string res;
  const string nums = "1234567890";
  int len = nums.size();
  while (res.size() < length)
  {
    int index = getRandomNumber(0, len - 1);
    res += nums[index];
  }
  return res;
}

string usernamegenerator(int uLen = 12)
{
  int maxIter = 0;
  int pNums = getRandomNumber(floor(uLen / 3), ceil(uLen / 2)), numCaps = getRandomNumber(floor(uLen / 6), ceil(uLen / 3));
  string username = "", insertUpper = "", insertNum = "";
  const string lowerCharacters = "abcedfghijklmnopqrstuvwxyz";

  int lenLower = lowerCharacters.size();
  insertUpper = handleCaps(numCaps);
  insertNum = handleNums(pNums);
  while (username.size() != uLen)
  {
    if (maxIter >= 250)
    {
      cout << "Something went wrong, max retries exceeded, please relaunch and try again" << endl;
      break;
    }
    int numindx = getRandomNumber(0, insertNum.size() - 1);
    int capsindex = getRandomNumber(0, insertUpper.size() - 1);
    int choice = getRandomNumber(1, 2);
    int lowerindex = getRandomNumber(0, lenLower - 1);
    if (insertUpper == "")
    {
      choice = 1;
    }
    else if (insertNum == "")
    {
      choice = 2;
    }

    if (username.size() == uLen)
    {
      break;
    }
    username += lowerCharacters[lowerindex];
    if (!insertUpper.empty() && choice == 1)
    {
      username += insertUpper[capsindex];
      insertUpper.erase(capsindex, 1);
    }
    if (!insertNum.empty() && choice == 2)
    {
      username += insertNum[numindx];
      insertNum.erase(numindx, 1);
    }
  }
  return username;
}