#include "../numbergenerator/numbergenerator.h"
#include <string>
#include <cmath>
#include <iostream>

using std::cout;
using std::endl;
using std::string;
using std::ceil;
using std::floor;


string handleSpecialCharacters(int length)
{
  string res;
  const string specials = "!@#$%^&*_-";
  int len = specials.size();
  while (res.size() < length)
  {
    int index = getRandomNumber(0, len - 1);
    res += specials[index];
  }
  return res;
}

string handleCapitalCharacters(int length)
{
  string res;
  const string capitals = "ABCEDFGHIJKLMNOPQRTSTUVWXYZ";
  int len = capitals.size();
  while (res.size() < length)
  {
    int index = getRandomNumber(0, len - 1);
    res += capitals[index];
  }
  return res;
}

string handleNumbers(int length)
{
  string res;
  const string nums = "1234567890";
  int len = nums.size();
  while (res.size() < length)
  {
    int index = getRandomNumber(0, len - 1);
    res += nums[index];
  }
  return res;
}

string passwordgenerator(int pLen = 12)
{
  int maxIter = 0;
  int spChar = getRandomNumber(floor(pLen / 12), floor(pLen / 6)), pNums = getRandomNumber(floor(pLen / 12), ceil(pLen / 6)), numCaps = getRandomNumber(floor(pLen / 6), ceil(pLen / 3));
  string password = "", insertUpper = "", insertSpecial = "", insertNum = "";
  const string lowerCharacters = "abcedfghijklmnopqrstuvwxyz";


  int lenLower = lowerCharacters.size();
  insertUpper = handleCapitalCharacters(numCaps);
  insertSpecial = handleSpecialCharacters(spChar);
  insertNum = handleNumbers(pNums);
  while (password.size() != pLen)
  {
    if (maxIter >= 250)
    {
      cout << "Something went wrong, max retries exceeded, please relaunch and try again" << endl;
      break;
    }
    int numindx = getRandomNumber(0, insertNum.size() - 1);
    int capsindex = getRandomNumber(0, insertUpper.size() - 1);
    int specialindx = getRandomNumber(0, insertSpecial.size() - 1);
    int choice = getRandomNumber(1, 3);
    int lowerindex = getRandomNumber(0, lenLower - 1);
    if (insertUpper == "")
    {
      choice = getRandomNumber(1, 2);
    }
    else if (insertSpecial == "")
    {
      choice = getRandomNumber(2, 3);
    }
    else if (insertNum == "")
    {
      choice = getRandomNumber(1, 2);
      if (choice == 2)
        choice++;
    }

    if (password.size() == pLen)
    {
      break;
    }
    password += lowerCharacters[lowerindex];
    if (!insertUpper.empty() && choice == 3)
    {
      password += insertUpper[capsindex];
      insertUpper.erase(capsindex, 1);
    }
    if (!insertSpecial.empty() && choice == 1)
    {
      password += insertSpecial[specialindx];
      insertSpecial.erase(specialindx, 1);
    }
    if (!insertNum.empty() && choice == 2)
    {
      password += insertNum[numindx];
      insertNum.erase(numindx, 1);
    }
  }
  return password;
}