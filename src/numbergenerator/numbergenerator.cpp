#include "numbergenerator.h"
#include <random>
int getRandomNumber(int minValue, int maxValue)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<int> distribution(minValue, maxValue);
  int randomNumber = distribution(gen);
  return randomNumber;
}