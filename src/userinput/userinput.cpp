#include "userinput.h"
#include "../../nlohmann/json.hpp"
#include <string>
#include <fstream>

void LoginGenerator::run(int argc, char *argv[])
{
  if (argc > 1)
  {
    std::string command = argv[1];
    if (command == "generate")
    {
      handleGenerateCommand(argc, argv);
    }
    else if (command == "grab")
      handleGenerateCommand(argc, argv);
    else
    {
      std::cout << "Invalid command." << std::endl;
      std::cout << "Example format is LoginGenerator generate -ul 16 -pl 16 -c company_name" << std::endl;
      system("pause");
    }
  }
  else
  {
    handleUserInput();
  }
}

void LoginGenerator::saveCredentials(std::string username = "username", std::string password = "password", std::string company = "Comapany_Name")
{
  nlohmann::json jsonFile = initJson();
  int id = jsonFile["Current_ID"];
  ++id;

  nlohmann::json &accounts = jsonFile["Accounts"];

  nlohmann::json newAccount;
  newAccount["id"] = id;
  newAccount["username"] = username;
  newAccount["password"] = password;

  accounts[company] = newAccount;
  jsonFile["Current_ID"] = id;

  std::ofstream outputFile("data/Logins.json");
  outputFile << jsonFile.dump(4);

  std::cout << "New account added to your Accounts" << std::endl;
}

void LoginGenerator::grabCredentials(std::string company)
{
  nlohmann::json jsonFile = initJson();
  nlohmann::json account = jsonFile["Accounts"][company];
  std::string username, password;
  username = account["username"];
  password = account["password"];
  std::cout << "username: " << username << std::endl;
  std::cout << "password: " << password << std::endl;
}

nlohmann::json LoginGenerator::initJson()
{
  nlohmann::json jsonFile;
  std::ifstream inputFile("data/Logins.json");
  inputFile >> jsonFile;
  return jsonFile;
}

void LoginGenerator::generateCredentials(int usernamelength, int passwordlength, std::string &username, std::string &password)
{
  username = usernamegenerator(usernamelength);
  password = passwordgenerator(passwordlength);
  std::cout << "Generated username: " << username << std::endl;
  std::cout << "Generated password: " << password << std::endl;
  if (username.empty() || password.empty())
    system("pause");
}

void LoginGenerator::handleGenerateCommand(int argc, char *argv[])
{
  int passwordlength = 12;
  int usernamelength = 12;
  std::string company;
  std::string username, password;
  if ((std::strcmp(argv[2], "-c") == 0))
  {
    for (int i = 3; i < argc; i++)
    {
      company += argv[i];
      if (i < argc - 1)
      {
        company += " ";
      }
    }
    std::cout << company << std::endl; 
    if (std::strcmp(argv[2], "-c") == 0)
      grabCredentials(company);
  }
  else if ((std::strcmp(argv[3], "-ul") == 0))
  {
    for (int i = 8; i < argc; i++)
    {
      company += argv[i];
      if (i < argc - 1)
      {
        company += " ";
      }
    }
    if (std::strcmp(argv[3], "-ul") == 0)
      usernamelength = std::stoi(argv[4]);
    if (std::strcmp(argv[5], "-pl") == 0)
      passwordlength = std::stoi(argv[6]);
    if (std::strcmp(argv[7], "-c") == 0)
    {
      generateCredentials(usernamelength, passwordlength, username, password);
      saveCredentials(username, password, company);
    }
  }
  else if (std::strcmp(argv[2], "-u") == 0 || std::strcmp(argv[2], "-p") == 0)
  {
    if (std::strcmp(argv[2], "-u") == 0 && std::strcmp(argv[3], "-l") == 0)
    {
      usernamelength = std::stoi(argv[4]);
      std::string res = usernamegenerator(usernamelength);
      std::cout << "Generated username: " << res << std::endl;
      if (res.empty())
        system("pause");
    }
    else if (std::strcmp(argv[2], "-p") == 0 && std::strcmp(argv[3], "-l") == 0)
    {
      passwordlength = std::stoi(argv[4]);
      std::string res = passwordgenerator(passwordlength);
      std::cout << "Generated password: " << res << std::endl;
      if (res.empty())
        system("pause");
    }
  }
  else
  {
    std::cout << "Invalid command." << std::endl;
    std::cout << "Example format is LoginGenerator generate -ul 16 -pl 16 -c company_name" << std::endl;
    system("pause");
  }
}


void LoginGenerator::handleUserInput()
{
  std::cout << "What would you like to do? (\"generate\", \"quit\" or \"grab\")" << std::endl;

  std::string userInput;
  while (getline(std::cin, userInput))
  {
    if (userInput == "generate")
    {
      std::string choice;
      std::cout << "Would you like to make a username, password, or both?" << std::endl;
      getline(std::cin, choice);
      if (choice == "password")
      {
        int length = 12;
        std::string company;
        std::string choice;
        std::cout << "Enter the company you are creating this for: " << std::endl;
        getline(std::cin, company);
        std::cin.ignore();
        std::cout << "Enter the length of the password: ";
        std::cin >> length;
        std::cin.ignore(); // Ignore the remaining newline character
        std::string password = passwordgenerator(length);
        std::cout << "Generated password: " << password << std::endl;
        std::cout << "Would you like to save this password? (Y/N)" << std::endl;
        std::cin >> choice;
        std::cin.ignore();
        if (choice == "Y" || choice == "y") {
          saveCredentials(password, company);
          std::cout << "password saved under company: " + company << std::endl;
        }
        system("pause");
      }
      else if (choice == "username")
      {
        int length = 12;
        std::string company;
        std::cout << "Enter the company you are creating this for: " << std::endl;
        getline(std::cin, company);
        std::cout << "Enter the length of the username: ";
        std::cin >> length;
        std::cin.ignore(); // Ignore the remaining newline character
        std::string username = usernamegenerator(length);
        std::cout << "Generated username: " << username << std::endl;
        std::cout << "Would you like to save this username? (Y/N)" << std::endl;
        std::cin >> choice;
        std::cin.ignore();
        if (choice == "Y" || choice == "y")
        {
          saveCredentials(username, company);
          std::cout << "username saved under company: " + company << std::endl;
        }
        system("pause");
      }
      else if (choice == "both")
      {
        int passwordlength = 12;
        int usernamelength = 12;
        std::string company;
        std::cout << "Enter the company you are creating this for: " << std::endl;
        getline(std::cin, company);
        std::cout << "Enter the length of the username: ";
        std::cin >> usernamelength;
        std::cin.ignore();
        std::cout << "Enter the length of the password: ";
        std::cin >> passwordlength;
        std::cin.ignore();
        std::string username, password;
        generateCredentials(usernamelength, passwordlength, username, password);
        std::cout << "Would you like to save this username & password? (Y/N)" << std::endl;
        std::cin >> choice;
        std::cin.ignore();
        if (choice == "Y" || choice == "y")
        {
          saveCredentials(username, password, company);
          std::cout << "username & password saved under company: " + company << std::endl;
        }
        system("pause");
      }
      
      else
      {
        std::cout << "Invalid choice." << std::endl;
        system("pause");
      }
    }
    else if (userInput == "quit")
    {
      std::cout << "Exiting the program." << std::endl;
      system("pause");
      break;
    }
    else if (userInput == "grab")
    {
      std::string company;
      std::cout << "Enter the company name to search for an account: " << std::endl;
      std::cin >> company;
      std::cin.ignore();
      grabCredentials(company);
      system("pause");
    }
    else
    {
      std::cout << "Invalid command." << std::endl;
      system("pause");
    }
    std::cout << "What would you like to do? (\"generate\", \"quit\" or \"grab\")" << std::endl;
  }
}