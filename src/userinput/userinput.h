#pragma once

#include <iostream>
#include <cstring>
#include <string>
#include "../UsernameGenerator/usernamegenerator.h"
#include "../../nlohmann/json.hpp"
#include "../PasswordGenerator/passwordgenerator.h"

class LoginGenerator
{
public:
  void run(int argc, char *argv[]);

private:
  void generateCredentials(int usernamelength, int passwordlength, std::string &username, std::string &password);
  void handleGenerateCommand(int argc, char *argv[]);
  void handleUserInput();
  void saveCredentials(std::string username, std::string password, std::string company);
  void grabCredentials(std::string company);
  nlohmann::json initJson();
};