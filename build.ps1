$rootDirectory = Resolve-Path -Path ..\..\cpp\LoginGenerator
$outputFile = Join-Path -Path $rootDirectory -ChildPath "LoginGenerator.exe"
$sourceFiles = Get-ChildItem -Path $rootDirectory\src -Filter "*.cpp" -Recurse

$sourceFilesPaths = $sourceFiles.FullName -join " "

$nlohmannDirectory = Join-Path -Path $rootDirectory -ChildPath "nlohmann"

$includeFlags = "-I`"$nlohmannDirectory`""
$includeFlags += Get-ChildItem -Path $nlohmannDirectory -Filter "*.hpp" -Recurse | ForEach-Object { "-I`"$($_.DirectoryName)`"" }

$compileCommand = "g++ -std=c++17 -o `"$outputFile`" -I`"$rootDirectory`" $includeFlags $sourceFilesPaths"

Write-Host "Building LoginGenerator..."

Invoke-Expression -Command $compileCommand

# Create Logins.json file if it doesn't exist
$jsonFilePath = Join-Path -Path $rootDirectory -ChildPath "data\Logins.json"
if (-not (Test-Path $jsonFilePath)) {
  $defaultJsonContent = @"
{
    "Current_ID": 0,
    "Accounts": {}
}
"@
  $defaultJsonContent | Set-Content -Path $jsonFilePath
}

Write-Host "Build complete."