# LoginGen+

To run project cd into the cloned repository from your terminal
and run build.ps1 in powershell or for bash terminal run `chmod +x build.sh`
in the proj directory followed by `./build.sh`

## Documentation

- Valid commands are currently `generate` and `quit` and `grab` and are to be done inside of the project directory
- `generate` has 6 flags `-up`, `-u`, `-p` where `-u` generates a username
  `-p` generates a password and `-up` signifies both
- the other 3 flags are `-ul <int>` `-pl <int>` and `-l <int>` where
  `-ul` signifies the length of the username followed by the integer you desire
  `-pl` signifies the length of the password followed by the integer you desire
  `-l` is only used when you use `-p` or `-u` and not `-up`, followed by the integer you desire
  `-c` is used in `grab` and `generate` to specify a company to save the username and password to if you choose to
- `grab` has one flag `-c` and and is followed by the company you stored the account under
- Example Commands:
  - `./LoginGenerator generate -up -ul 10 -pl 16 -c company_name`
  - `./LoginGenerator generate -p -l 16`
  - `./LoginGenerator generate -u -l 16`
